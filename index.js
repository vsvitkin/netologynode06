/************************
Vasily Svitkin 2017
Netology NodeJS course
Home work 6
*/
const port=5000;
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

let UsersDB = require('./usersDB').UsersDB;
let DB = new UsersDB();
DB.addDemoData(10);



app.listen(port,()=>{
  console.log(`Server started on ${port} port...`);
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':true}));


//RPC
////////////////////////////////////////////////////////////////////////////////


function getRPCError(text,id=0){
  return {'jsonrpc':'2.0', 'error':{'code':'-1', 'message':text}, 'id': id}
}
function getRPCAnswer(data,id){
  return {'jsonrpc':'2.0', 'result':data, 'id': id}
}


app.use('/rpc', function(req,res,next){
  let message='';
  if (req.body.jsonrpc!='2.0'){
    message+='Only jsonrpc 2.0 is supported. ';
  }
  if (!['list','show','add','update','delete'].includes(req.body.method)){
    message+='Method is not supported. ';
  }
  if (!req.body.id){
    message+='Request ID not found. ';
  }
  if (['show', 'update', 'delete'].includes(req.body.method)
    &&(!req.body.params||!req.body.params.id||!DB.userExists(req.body.params.id))){
        message+='User not found. ';
  }
  if (['add','update'].includes(req.body.method)&&(!req.body.params.name||!req.body.params.score)){
        message+='Name and score are required for this operation. ';
  }
  if (message!=''){
    return res.status(400).json(getRPCError(message,req.body.id?req.body.id:0));
  }
  next();
});


app.post('/rpc',(req,res)=>{
  switch (req.body.method) {
    case 'list':
    res.json(getRPCAnswer(DB.getAll(), req.body.id));
    break;
    case 'show':
    res.json(getRPCAnswer(DB.getUser(req.body.params.id),req.body.id));
    break;
    case 'add':
    res.json(getRPCAnswer(DB.addUser(req.body.params.name,req.body.params.score),req.body.id));
    break;
    case 'update':
    res.json(getRPCAnswer(DB.updateUser(req.body.params.id,req.body.params.name,req.body.params.score),req.body.id));
    break;
    case 'delete':
    res.json(getRPCAnswer(DB.deleteUser(req.body.params.id),req.body.id));
    break;
  }
});
///REST
////////////////////////////////////////////////////////////////////////////////
//Получение списка пользователей
app.get('/users', (req,res)=>{
  res.json(DB.getAll());
});

////////////////////////////////////////////////////////////////////////////////
//Добавление пользователя
app.post('/users', (req,res)=>{
  if (!req.body.name||!req.body.score){
    return res.status(400).json({'error':'Name and score are required for this operation'});
  }
  let id = DB.addUser(req.body.name, req.body.score);
  res.json({'id':id});
});


//Для всех остальных методов проверяем валидность пользователя
app.use('/users/:id',function(req,res,next){
  if (!DB.userExists(req.params.id)){
    return res.status(404).json({'error':'User not found'});
  }
  next();
});

////////////////////////////////////////////////////////////////////////////////
//Получение пользователя
app.get('/users/:id', (req,res)=>{
  let result = {'id':req.params.id,
                'user':DB.getUser(req.params.id)};
  res.json(result);
});

////////////////////////////////////////////////////////////////////////////////
//Редактирование пользователя
app.put('/users/:id', (req,res)=>{
  if (!req.body.name||!req.body.score){
    return res.status(400).json({'error':'Name and score are required for this operation'});
  }
  DB.updateUser(req.params.id,req.body.name,req.body.score);
  res.json({'id':req.params.id});
});

////////////////////////////////////////////////////////////////////////////////
//Удаление пользвователя
app.delete('/users/:id', (req,res)=>{
  DB.deleteUser(req.params.id);
  res.json({});
});


app.all('*', (req,res)=>{
  res.status(404).json({'error':'method not found'});
});
